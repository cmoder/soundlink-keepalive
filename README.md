SoundLink Keepalive
===================

The Bose SoundLink is a convenient speaker for PCs. However, a timeout switches
it off after some time of inactivity. This timemout cannot be deactivated; thus
I tried to create some kind of keepalive signal that keeps the speaker switched
on, but is almost not noticeable. So I wrote this simple script that is called
every 15 minutes by */etc/crontab*. It checks whether the speaker is in use;
if not, a short signal is sent to keep it active.
